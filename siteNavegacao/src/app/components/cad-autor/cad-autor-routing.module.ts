import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadAutorComponent } from './cad-autor.component';

const routes: Routes = [
  {
    path: "",
    component: CadAutorComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadAutorRoutingModule { }
